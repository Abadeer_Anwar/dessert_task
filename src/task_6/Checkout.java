package task_6;

import java.util.ArrayList;

public class Checkout {

	 ArrayList<DessertItem>order;

	public Checkout() 
	{
		order = new ArrayList<DessertItem>();
	}
	
	public int numberOfItems()
	{ 
		return order.size();
	}
	
	public void enterItem(DessertItem item)
	{
		order.add(item);
	}
	
	public void clear()
	{
		order.clear();
	}
	
	public int totalCost()
	{
		float totalCost=0;
		for(int i=0;i<order.size();i++)
		{ 
			totalCost+=order.get(i).getCost();
		}
		return (int) totalCost;
	}
	
	public int totalTax()
	{			
		float totalCost=0;
		for(int i=0;i<order.size();i++)
		{ 
			totalCost+=order.get(i).getCost();
		}
		return (int) (totalCost*DessertShoppe.TAX_RATE);
	}
	
	public java.lang.String toString()
	{
		String s="";
		s+=DessertShoppe.STORE_NAME;
		s+="\n-----------------------\n";
		for(int i=0;i<order.size();i++)
		{
			s+=      order.get(i).getName();
			s+="\t"+ order.get(i).getCost()+ "\n";
		}
		s+="tax  :\t"+ totalTax() + "\n";
		s+="Cost :\t"+ totalCost() + "\n";
		return s;

		
	}
}
