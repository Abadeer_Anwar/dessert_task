package task_6;

public class Cookie extends DessertItem{
	private int number;
	private float price;

	public Cookie(String name, int number, float price) {
		super(name);
		this.number = number;
		this.price = price;
	}

	@Override
	float getCost() 
	{
		return  (number*price);
	}

	
}
