package task_6;

public abstract class DessertItem {
	private int dataMember;
	private String name;

	public DessertItem( String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	abstract float getCost();

}
