package task_6;

public class Candy extends DessertItem{

	private float weight;
	private float price;
	public Candy(String name, float weight, float price) {
		super(name);
		this.weight = weight;
		this.price = price;
	}
	


	@Override
	float getCost() {
		// TODO Auto-generated method stub
		return (weight*price);
	}

}
