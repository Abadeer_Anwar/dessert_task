package task_6;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Checkout checkout = new Checkout();
	    
	    checkout.enterItem(new Candy("Peanut Butter Fudge", 2.25f, 30));
	    checkout.enterItem(new Icecream("Vanilla Ice Cream",105));
	    checkout.enterItem(new Sundae("Choc. Chip Ice Cream", 50));
	    checkout.enterItem(new Cookie("Oatmeal Raisin Cookies", 4, 399));
	    
	    System.out.println("\nNumber of items: " + checkout.numberOfItems() + "\n");
	    System.out.println("\nTotal cost: " + checkout.totalCost() + "\n");
	    System.out.println("\nTotal tax: " + checkout.totalTax() + "\n");
	    System.out.println("\nCost + Tax: " + (checkout.totalCost() + checkout.totalTax()) + "\n");
	    System.out.println(checkout);
	    
	    checkout.clear();
	    
	    checkout.enterItem(new Icecream("Strawberry Ice Cream",145));
	    checkout.enterItem(new Sundae("Vanilla Ice Cream", 50));
	    checkout.enterItem(new Candy("Gummy Worms",.33f, 89));
	    checkout.enterItem(new Cookie("Chocolate Chip Cookies", 4, 399));
	    checkout.enterItem(new Candy("Salt Water Taffy", 1.5f, 209));
	    checkout.enterItem(new Candy("Candy Corn",3.0f, 109));
	    
	    System.out.println("\nNumber of items: " + checkout.numberOfItems() + "\n");
	    System.out.println("\nTotal cost: " + checkout.totalCost() + "\n");
	    System.out.println("\nTotal tax: " + checkout.totalTax() + "\n");
	    System.out.println("\nCost + Tax: " + (checkout.totalCost() + checkout.totalTax()) + "\n");
	    System.out.println(checkout);
	}

}
