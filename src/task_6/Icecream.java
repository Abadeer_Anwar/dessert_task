package task_6;

public class Icecream extends DessertItem{
	private float price;
	
	public Icecream(String name, float price) {
		super(name);
		this.price = price;
	}

	@Override
	float getCost() {
		// TODO Auto-generated method stub
		return price;
	}
	
}
